﻿using System;

class Ponger
{
    static void Main(string[] args)
    {
        Console.WriteLine("ponger started (press [enter] to exit)");

        Wrapper rabbit = new Wrapper();
        rabbit.ListenQueue("pong_queue");

        Console.ReadLine();
        rabbit.CloseConnection();
    }
}