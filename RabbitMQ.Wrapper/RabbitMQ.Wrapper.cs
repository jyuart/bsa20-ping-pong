﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

public class Wrapper
{
    public readonly IConnection connection;
    public readonly IModel channel;
    public readonly EventingBasicConsumer consumer;

public Wrapper()
    {
        var factory = new ConnectionFactory() { HostName = "localhost" };

        connection = factory.CreateConnection();
        channel = connection.CreateModel();
        consumer = new EventingBasicConsumer(channel);

        consumer.Received += (model, ea) =>
        {
            // Printing received message 
            var received_message = Encoding.UTF8.GetString(ea.Body.ToArray());
            Console.WriteLine($"[x] Received {received_message} at {DateTime.Now}");

            // Generating message to send back
            var text_to_send = received_message == "ping" ? "pong" : "ping";
            var message = Encoding.UTF8.GetBytes(text_to_send);

            // Waiting
            Thread.Sleep(2500);

            // Declaring queue for sending message back
            var queue_name = ea.RoutingKey == "ping_queue" ? "pong_queue" : "ping_queue";
            channel.QueueDeclare(queue: queue_name, exclusive: false);

            // Sending message
            channel.BasicPublish(exchange: "", routingKey: queue_name, basicProperties: null, body: message);
            Console.WriteLine($"[x] Sent {text_to_send} at {DateTime.Now}");
        };
    }

    public void ListenQueue(string queue_name)
    {
        channel.QueueDeclare(queue_name, exclusive: false);
        channel.BasicConsume(queue_name, autoAck: true, consumer: consumer);
    }

    public void SendMessageToQueue(string queue_name, string message)
    {
        channel.QueueDeclare(queue: queue_name, exclusive: false);
        var msg = Encoding.UTF8.GetBytes(message);
        channel.BasicPublish(exchange: "", routingKey: queue_name, body: msg);
        Console.WriteLine($"[x] Sent {message} at {DateTime.Now}");
    }

    public void CloseConnection()
    {
        connection.Close();
    }
}

