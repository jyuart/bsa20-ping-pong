﻿using System;

class Pinger
{
    static void Main(string[] args)
    {
        Console.WriteLine("pinger started (press [enter] to exit)");

        Wrapper rabbit = new Wrapper();
        rabbit.SendMessageToQueue("pong_queue", "ping");
        rabbit.ListenQueue("ping_queue");

        Console.ReadLine();
        rabbit.CloseConnection();
    }
}